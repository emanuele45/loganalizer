/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LogAnalyzer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author emanuele
 */
public class LogLine {
	/**
	 * @todo later I would likely support custom log formats,
	 * for the moment I will go with a default one
	 */
	public String logFormat = "%h %l %u %t \"%r\" %>s %b";
	private String _originalEntry;
	private String _IP;
	private String _UID;
	private Date _date;
	private int _zone;
	private String _method;
	private NetAddress _address;
	private String _protocol;
	private statusCode _statusCode;
	private int _objectSize;
	private NetAddress _referer;
	private String _UserAgent = "";

	private static String[] _validMethods = {
    "GET",
    "POST",
    "HEAD",
    "OPTIONS",
    "PUT",
    "DELETE",
    "TRACE",
    "CONNECT"
	};
	private static HashMap<Integer, String> _validStatusCodes = new HashMap<>();

	/*
	 * This is a sample line
	 99.98.149.232 - - [18/Sep/2013:05:13:57 -0700] "GET /index.php?action=keepalive;time=1379506441547 HTTP/1.0" 200 57 "http://warriorcatsrpg.com/index.php?topic=1169757.msg53828475" "Mozilla/5.0 (Windows NT 5.1; rv:23.0) Gecko/20100101 Firefox/23.0"
	 * -- ** splitting ** -- *
	 0) 99.98.149.232
	 1) -
	 2) - 
	 3) [18/Sep/2013:05:13:57
	 4) -0700]
	 5) "GET
	 6) /index.php?action=keepalive;time=1379506441547
	 7) HTTP/1.0"
	 8) 200
	 9) 57
	 10) "http://warriorcatsrpg.com/index.php?topic=1169757.msg53828475"
	 11+) "Mozilla/5.0 (Windows NT 5.1; rv:23.0) Gecko/20100101 Firefox/23.0"
	 */
	
	public LogLine(String logLine) throws ParseException {
		StringBuilder UA = new StringBuilder();
		boolean hasUA = false;

		if (!logFilter.matches(logLine))
			return;

		this._originalEntry = logLine;
		String[] lineSplit = logLine.split(" ");
		String[] logformatSplit = this.logFormat.split(" ");
		int i;

		// Ensure the array is sorted
		Arrays.sort(LogLine._validMethods);
		this._generateValidStatusCodes();
		
		for (i = 0; i < logformatSplit.length && i < lineSplit.length; i++) {
			if (!lineSplit[i].equals("-")) {
				// Yes, I know, it's not the way to do it, but for now it works
				switch (i) {
					case 0:
						this._IP = _cleanIP(lineSplit[i]);
						break;
					// case 1: %l => Remote logname (usually not present and at the moment I don't care about things not present)
					case 2:
						this._UID = lineSplit[i];
						break;
					case 3:
						String cleanDate = lineSplit[i].trim().replaceAll("[\\[\\]]", "");
						this._date = new SimpleDateFormat("ww/MMM/YYYY:kk:mm:ss", Locale.ENGLISH).parse(cleanDate);
						break;
					case 4:
						this._zone = _cleanZone(lineSplit[i]);
						break;
					case 5:
						this._method = _cleanMethod(lineSplit[i]);
						break;
					case 6:
						this._address = new NetAddress(lineSplit[i]);
						break;
					case 7:
						this._protocol = _cleanProtocol(lineSplit[i]);
						break;
					case 8:
						this._statusCode = _cleanStatusCode(lineSplit[i]);
						break;
					case 9:
						this._objectSize = Integer.parseInt(lineSplit[i].trim());
						break;
					case 10:
						this._referer = new NetAddress(lineSplit[i]);
						break;
					default:
						UA.append(lineSplit[i]);
						hasUA = true;
						break;
				}
			}
		}
		if (hasUA)
			this._UserAgent = UA.toString();
		// @todo it may be useless
		else
			this._UserAgent = "";
	}

	private statusCode _cleanStatusCode(String sStatusCode) {
			statusCode a = new statusCode();
			if (LogLine._validStatusCodes.get(Integer.parseInt(sStatusCode)) != null) {
				a.code = Integer.parseInt(sStatusCode);
				a.description = LogLine._validStatusCodes.get(a.code);
			}
			else {
				a.code = Integer.parseInt(sStatusCode);
				a.description = "Unknown code";
			}
			return a;
	}

	private String _cleanProtocol(String sProtocol) {
		return sProtocol.trim().replaceAll("\"", "");
	}

	private String _cleanMethod(String sMethod) {
		sMethod = sMethod.trim().replaceAll("\"", "");
		
		// If it is one of the methods we know, return it, otherwise nothing.
		if (Arrays.binarySearch(LogLine._validMethods, sMethod) > 0) {
			return sMethod;
		}
		else {
			return "";
		}
	}

	/**
	 * The zone is in the format (`+' | `-') 4*digit
	 * The function validates the passed argument, then
	 * it splits it up and return an integer as minutes offsets
	 *
	 * @param _offset
	 * @return integer +/- in minutes
	 */
	private int _cleanZone(String sZone) {
		sZone = sZone.trim().replaceAll("[\\[\\]]", "");
		Pattern p = Pattern.compile("[+|-]\\d{4}");
		Matcher m = p.matcher(sZone);
		if (m.matches()) {
			char direction = sZone.charAt(0);
			int hours = Integer.parseInt(sZone.substring(1, 3));
			int minutes = Integer.parseInt(sZone.substring(3));
			return Integer.parseInt("" + direction + Integer.toString(hours * 60 + minutes));
		}
		else {
			return 0;
		}
	}

	private String _cleanIP(String sIP) {
		Pattern p = Pattern.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");
		Matcher m = p.matcher(sIP);
		if (m.matches())
			return sIP;
		else
			return "0.0.0.0";
	}

	/**
	 * ref: http://www.w3.org/Protocols/rfc2616/rfc2616.txt
	 */
	private void _generateValidStatusCodes() {
		LogLine._validStatusCodes.put(100, "Continue");
		LogLine._validStatusCodes.put(101, "Switching Protocols");
		LogLine._validStatusCodes.put(200, "OK");
		LogLine._validStatusCodes.put(201, "Created");
		LogLine._validStatusCodes.put(202, "Accepted");
		LogLine._validStatusCodes.put(203, "Non-Authoritative Information");
		LogLine._validStatusCodes.put(204, "No Content");
		LogLine._validStatusCodes.put(205, "Reset Content");
		LogLine._validStatusCodes.put(206, "Partial Content");
		LogLine._validStatusCodes.put(300, "Multiple Choices");
		LogLine._validStatusCodes.put(301, "Moved Permanently");
		LogLine._validStatusCodes.put(302, "Found");
		LogLine._validStatusCodes.put(303, "See Other");
		LogLine._validStatusCodes.put(304, "Not Modified");
		LogLine._validStatusCodes.put(305, "Use Proxy");
		LogLine._validStatusCodes.put(307, "Temporary Redirect");
		LogLine._validStatusCodes.put(400, "Bad Request");
		LogLine._validStatusCodes.put(401, "Unauthorized");
		LogLine._validStatusCodes.put(402, "Payment Required");
		LogLine._validStatusCodes.put(403, "Forbidden");
		LogLine._validStatusCodes.put(404, "Not Found");
		LogLine._validStatusCodes.put(405, "Method Not Allowed");
		LogLine._validStatusCodes.put(406, "Not Acceptable");
		LogLine._validStatusCodes.put(407, "Proxy Authentication Required");
		LogLine._validStatusCodes.put(408, "Request Time-out");
		LogLine._validStatusCodes.put(409, "Conflict");
		LogLine._validStatusCodes.put(410, "Gone");
		LogLine._validStatusCodes.put(411, "Length Required");
		LogLine._validStatusCodes.put(412, "Precondition Failed");
		LogLine._validStatusCodes.put(413, "Request Entity Too Large");
		LogLine._validStatusCodes.put(414, "Request-URI Too Large");
		LogLine._validStatusCodes.put(415, "Unsupported Media Type");
		LogLine._validStatusCodes.put(416, "Requested range not satisfiable");
		LogLine._validStatusCodes.put(417, "Expectation Failed");
		LogLine._validStatusCodes.put(500, "Internal Server Error");
		LogLine._validStatusCodes.put(501, "Not Implemented");
		LogLine._validStatusCodes.put(502, "Bad Gateway");
		LogLine._validStatusCodes.put(503, "Service Unavailable");
		LogLine._validStatusCodes.put(504, "Gateway Time-out");
		LogLine._validStatusCodes.put(505, "HTTP Version not supported");
	}
}

class statusCode {
	public int code;
	public String description;
}

class logFilter {
	private static List<Pattern> _includePatterns = new ArrayList<>();
	private static List<Pattern> _excludePatterns = new ArrayList<>();

	private static boolean _includePattersSet = false;
	private static boolean _excludePattersSet = false;

	public static void include(Pattern p) {
		logFilter._includePatterns.add(p);
		logFilter._includePattersSet = true;
	}
	public static void include(String sPattern) {
		Pattern p = Pattern.compile(sPattern);
		logFilter.include(p);
	}

	public static void exclude(Pattern p) {
		logFilter._excludePatterns.add(p);
		logFilter._excludePattersSet = true;
	}
	public static void exclude(String sPattern) {
		Pattern p = Pattern.compile(sPattern);
		logFilter.exclude(p);
	}

	public static void remove(String fromWhere, Pattern p) {
		if (fromWhere.toLowerCase().equals("include")) {
			logFilter._includePatterns.remove(p);
			logFilter._includePattersSet = !logFilter._includePatterns.isEmpty();
		}
		else if (fromWhere.toLowerCase().equals("exclude")) {
			logFilter._excludePatterns.remove(p);
			logFilter._excludePattersSet = !logFilter._excludePatterns.isEmpty();
		}
	}
	public static void remove(String fromWhere, String sPattern) {
		Pattern p = Pattern.compile(sPattern);
		logFilter.remove(fromWhere, p);
	}

	public static boolean matches(String sMatch) {
		Pattern p;

		if (logFilter._excludePattersSet) {
			Iterator<Pattern> i = logFilter._excludePatterns.iterator();

			while (i.hasNext()) {
				p = (Pattern) i.next();
				if (p.matcher(sMatch).matches())
					return false;
			}
		}

		if (logFilter._includePattersSet) {
			Iterator<Pattern> i = logFilter._includePatterns.iterator();

			while (i.hasNext()) {
				p = (Pattern) i.next();
				if (p.matcher(sMatch).find())
					return true;
			}
			return false;
		}

		return true;
	}
}