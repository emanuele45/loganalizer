/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LogAnalyzer;

import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author emanuele
 */
public class LogAnalyzer {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		// TODO code application logic here
		LogAnalyzerWin MW = new LogAnalyzerWin();
		MW.setVisible(true);
	}

	public static void startAnalysis(String filePath) {
		int i;

		try {
			ReadFile myFile = new ReadFile(filePath);

			LogLine[] parsedLogs = new LogLine[myFile.getLines()];

			for (i = 0; myFile.hasNextLine(); i++) {
				parsedLogs[i] = new LogLine(myFile.readLastLine());
			}
		} catch (IOException | ParseException ex) {
			Logger.getLogger(LogAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
