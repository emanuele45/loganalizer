/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LogAnalyzer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author emanuele
 */
public class NetAddress {
	private String _address;
	private NetObject _netObject;

	public NetAddress(String sAddress) {
		this._address = sAddress;
	}

	public NetAddress(String sAddress, NetObject noPatterns) {
		this._address = sAddress;
		this._netObject = noPatterns;
	}

	public boolean matches(String sPattern) {
		Pattern p = Pattern.compile(sPattern);
		Matcher m = p.matcher(this._address);
		return m.matches();
	}
	public boolean matches() {
		try {
			return this._netObject.matches(this._address);
		}
		catch (NullPointerException e) {
			return false;
		}
	}
}
