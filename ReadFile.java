/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LogAnalyzer;

import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

/**
 *
 * @author emanuele
 */
public class ReadFile {
	private String _path;
	private boolean _isOpen = false;
	private BufferedReader _bf;
	private int _currentLine = 0;
	private String _currentLineContent;
	public int numberOfLines = -1;
	
	public ReadFile(String file_path) {
		this._path = file_path;
	}

	public boolean OpenFile() throws IOException {
		try {
			FileReader fr = new FileReader(this._path);
			this._bf = new BufferedReader(fr);
			this._isOpen = true;
			return true;
		}
		catch(IOException e) {
			this._isOpen = false;
			return false;
		}
	}

	public void CloseFile() throws IOException {
		this._bf.close();
		this._isOpen = false;
	}

	public boolean hasNextLine() throws IOException {
		if (!this._isOpen)
				this.OpenFile();

		if (this._currentLine < this.numberOfLines) {
			this._currentLine++;
			this._currentLineContent = this._bf.readLine();
			return true;
		}
		else
			return false;
	}

	public String readLastLine() {
		return this._currentLineContent;
	}

	public String[] readFile() throws IOException {
		int i;
		String[] textData;

		if (this.numberOfLines == -1)
			this._readLines();
		
		textData = new String[this.numberOfLines];

		for (i=0; i < this.numberOfLines; i++) {
			textData[i] = this._bf.readLine();
		}

		this.CloseFile();

		return textData;
	}

	public int getLines() throws IOException {
		if (this.numberOfLines == -1)
			this._readLines();

		return this.numberOfLines;
	}
	
	private void _readLines() throws IOException {
		String aLine;

		if (!this._isOpen)
				this.OpenFile();
		
		while ((aLine = this._bf.readLine()) != null) {
			this.numberOfLines++;
		}
		this.CloseFile();
	}
}
