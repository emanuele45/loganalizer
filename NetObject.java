/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LogAnalyzer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author emanuele
 */
public class NetObject {
	//private ArrayList patterns;
	private List<Pattern> patterns = new ArrayList<>();

	public void NetObject() {
		this.patterns = new ArrayList();
	}
	
	public void add(String sPattern) {
		Pattern p = Pattern.compile(sPattern);
		patterns.add(p);
	}

	public boolean matches(String sAddress) {
		Iterator<Pattern> i = this.patterns.iterator();

		while (i.hasNext()) {
			Pattern p = i.next();
			if (!p.matcher(sAddress).matches())
				return false;
		}
		return true;
	}
}
